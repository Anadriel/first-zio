package persistance

import entities.{DBError, EntityNotFoundError, User}
import persistence.UserDB
import zio.{Has, IO, Ref, ZLayer}

case class Test(users: Ref[(Long, Map[Long, User])]) extends UserDB {

  def create(entity: User): IO[DBError, User] =
    users.modify{ case (lastId, us) =>
      val newId = lastId + 1
      val resultEntity = entity.copy(id = Some(newId))
      resultEntity -> (newId, us + (newId -> resultEntity))
    }

  def get(entityId: Long): IO[DBError, User] =
    users.get.flatMap { case (_, users) =>
      IO.require(EntityNotFoundError(entityId))(IO.succeed(users.get(entityId)))
    }

  def update(entity: User): IO[DBError, Boolean] =
    for {
      ou <- users.get.map{
        case (_, users) =>
          entity.id.flatMap(users.get)
      }
      r <- ou.flatMap(_.id).map(uId => users.modify{
        case (lastId, users) =>
          true -> (lastId, users + (uId -> entity))
      }).getOrElse(IO.succeed(false))
    } yield r

  def delete(entityId: Long): IO[DBError, Boolean] = {
    for {
      ou <- users.get.map{
        case (_, users) =>
          users.get(entityId)
      }
      r <- ou.map(u => users.modify{
        case (lastId, users) =>
          true -> (lastId, users - entityId)
      }).getOrElse(IO.succeed(false))
    } yield r
  }

}

object Test {
  val layer: ZLayer[Any, Nothing, Has[UserDB]] =
    ZLayer.fromEffect(Ref.make(0L -> Map.empty[Long, User]).map(Test(_)))

}
