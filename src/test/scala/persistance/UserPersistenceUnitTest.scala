package persistance

import entities.{EntityNotFoundError, User}
import persistence.UserDB._
import zio.test.Assertion._
import zio.test._
import zio.test.environment.TestEnvironment

object UserPersistenceUnitTest extends DefaultRunnableSpec {

  def spec =
    suite("Persistence unit test")(
      testM("get a non existing user should fail") {
        assertM(get(100L).run)(fails(equalTo(EntityNotFoundError(100L))))
      },
      testM("create users with autoincrement ids and then get them") {
        for {
          created1 <- create(User(None, "some", "user", "tel"))
          user1 <- get(1L).either
          created2 <- create(User(None, "some2", "user2", "tel2"))
          user2 <- get(2L).either
        } yield assert(created1)(equalTo(User(Some(1L), "some", "user", "tel"))) &&
          assert(user1)(isRight(equalTo(User(Some(1L), "some", "user", "tel")))) &&
          assert(created2)(equalTo(User(Some(2L), "some2", "user2", "tel2"))) &&
          assert(user2)(isRight(equalTo(User(Some(2L), "some2", "user2", "tel2"))))
      },
      testM("update user") {
        for {
          created      <- create(User(None, "some", "user", "tel"))
          updated      <- update(User(Some(1L), "someNew", "userNew", "telNew"))
          updatedUser  <- get(1L).either
          notUpdated   <- update(User(Some(2L), "someNew", "userNew", "telNew"))
        } yield assert(created)(equalTo(User(Some(1L), "some", "user", "tel"))) &&
          assert(updated)(isTrue) &&
          assert(updatedUser)(isRight(equalTo(User(Some(1L), "someNew", "userNew", "telNew")))) &&
          assert(notUpdated)(isFalse)
      },
      testM("delete user") {
        for {
          created  <- create(User(None, "some", "user", "tel"))
          deleted  <- delete(1L)
          notFound <- get(1L).either
        } yield assert(created)(equalTo(User(Some(1L), "some", "user", "tel"))) &&
          assert(deleted)(isTrue) &&
          assert(notFound)(isLeft(hasMessage(equalTo("Entity with id 1 not found"))))
      }
    ).provideSomeLayer[TestEnvironment](Test.layer)
}
