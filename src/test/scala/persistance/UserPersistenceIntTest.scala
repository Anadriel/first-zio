package persistance

import persistence.{Postgres, UserDB}
import UserDB._
import config.Config
import entities.{EntityNotFoundError, User}
import zio.Cause
import zio.blocking.Blocking
import zio.test.Assertion._
import zio.test._
import zio.test.environment.TestEnvironment

object UserPersistenceIntTest extends DefaultRunnableSpec {

  def spec =
    suite("Persistence integration test")(testM("Persistence Live") {
      for {
        _                   <- Postgres.createUserTable
        notFound            <- get(100L).either
        created1            <- create(User(None, "some", "user", "tel")).either
        user1               <- get(1L).either
        created2            <- create(User(None, "some2", "user2", "tel2")).either
        user2               <- get(2L).either
        updated             <- update(User(Some(1L), "someNew", "userNew", "telNew"))
        updateNotExisted    <- update(User(Some(15L), "someNew", "userNew", "telNew"))
        deleted             <- delete(1L)
        deleteNotExisted    <- delete(15L)
        getDeleted          <- get(1L).either
        _                   <- Postgres.dropTable
      } yield assert(notFound)(isLeft(equalTo(EntityNotFoundError(100L)))) &&
        assert(created1)(isRight(equalTo(User(Some(1L), "some", "user", "tel")))) &&
        assert(user1)(isRight(equalTo(User(Some(1L), "some", "user", "tel")))) &&
        assert(created2)(isRight(equalTo(User(Some(2L), "some2", "user2", "tel2")))) &&
        assert(user2)(isRight(equalTo(User(Some(2L), "some2", "user2", "tel2")))) &&
        assert(updated)(isTrue) &&
        assert(updateNotExisted)(isFalse) &&
        assert(deleted)(isTrue) &&
        assert(deleteNotExisted)(isFalse) &&
        assert(getDeleted)(isLeft(equalTo(EntityNotFoundError(1L))))
    }).provideSomeLayer[TestEnvironment](
      (Config.live >+> Blocking.live >+> UserDB.postgresLive)
        .mapError(_ => TestFailure.Runtime(Cause.die(new Exception("die"))))
    )

}
