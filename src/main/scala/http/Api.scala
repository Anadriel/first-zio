package http

import entities.{EntityNotFoundError, User}
import io.circe.generic.auto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.{EntityDecoder, EntityEncoder, HttpRoutes}
import persistence.UserDB
import UserDB._
import zio._
import zio.interop.catz._

final case class Api[R <: Has[UserDB]]() {

  type UserTask[A] = RIO[R, A]

  implicit def circeJsonDecoder[A](implicit decoder: Decoder[A]): EntityDecoder[UserTask, A] =
    jsonOf[UserTask, A]
  implicit def circeJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[UserTask, A] =
    jsonEncoderOf[UserTask, A]

  val dsl: Http4sDsl[UserTask] = Http4sDsl[UserTask]
  import dsl._

  def route: HttpRoutes[UserTask] = {

    HttpRoutes.of[UserTask] {
      case GET -> Root / LongVar(id) =>
        get(id)
          .foldM(t => NotFound(t.getMessage), Ok(_))
      case request @ POST -> Root =>
        request.decode[User] { user =>
          Created(create(user))
        }
      case request @ PUT -> Root / LongVar(id) =>
        request.decode[User] { user =>
          update(user.copy(id = Some(id)))
            .flatMap(if(_) Ok() else NotFound(EntityNotFoundError(id).getMessage))
        }
      case DELETE -> Root / LongVar(id) =>
        delete(id)
          .flatMap(if(_) Ok() else NotFound(EntityNotFoundError(id).getMessage))
    }
  }

}