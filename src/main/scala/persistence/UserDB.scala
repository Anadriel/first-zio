package persistence

import config.Config.DBConfig
import entities.{DBError, User}
import persistence.DB.Service
import persistence.Postgres.PostgresDBEnvironment
import zio.blocking.Blocking
import zio.{Has, ZIO, ZLayer}


trait UserDB extends Service[User]

object UserDB {
  //val redis = ???

  val postgresLive: ZLayer[DBConfig with Blocking, Throwable, PostgresDBEnvironment] =
    Postgres.transactorLive >+> Postgres.live

 // val memory = ???

  def create(entity: User): ZIO[Has[UserDB], DBError, User] =
    ZIO.accessM(_.get.create(entity))

  def get(entityId: Long): ZIO[Has[UserDB], DBError, User] =
    ZIO.accessM(_.get.get(entityId))

  def update(entity: User): ZIO[Has[UserDB], DBError, Boolean] =
    ZIO.accessM(_.get.update(entity))

  def delete(entityId: Long): ZIO[Has[UserDB], DBError, Boolean] =
    ZIO.accessM(_.get.delete(entityId))
}
