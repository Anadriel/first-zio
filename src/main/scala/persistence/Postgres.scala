package persistence

import cats.effect.Blocker
import config.Config.DBConfig
import config.DatabaseConfig
import doobie.hikari.HikariTransactor
import doobie.implicits._
import doobie.{ConnectionIO, Transactor}
import entities._
import zio.blocking.{Blocking, blocking}
import zio._

import scala.concurrent.ExecutionContext

object Postgres {
  import zio.interop.catz.taskConcurrentInstance

  object SQL{
    def create(user: User): ConnectionIO[User] =
      sql"""INSERT INTO USERS (first_name, last_name, contacts)
           VALUES (${user.firstName}, ${user.lastName}, ${user.contacts})"""
        .update
        .withUniqueGeneratedKeys[User]("id", "first_name", "last_name", "contacts")

    def get(userId: Long): ConnectionIO[Option[User]] =
      sql"""SELECT * FROM USERS WHERE ID = $userId """
        .query[User]
        .option

    def update(user: User): ConnectionIO[Int] =
      sql"""UPDATE USERS SET first_name = ${user.firstName},
           last_name = ${user.lastName}, contacts = ${user.contacts} WHERE id = ${user.id}"""
        .update
        .run

    def delete(userId: Long): ConnectionIO[Int] =
      sql"""DELETE FROM USERS WHERE id = $userId"""
        .update
        .run

    def createUsersTable: ConnectionIO[Int] =
      sql"""
        CREATE TABLE IF NOT EXISTS USERS (
          id   BIGSERIAL,
          first_name TEXT NOT NULL,
          last_name TEXT NOT NULL,
          contacts TEXT NOT NULL,
          PRIMARY KEY (id)
        )
        """
        .update
        .run

    def dropTable: ConnectionIO[Int] =
      sql"""DROP TABLE IF EXISTS USERS"""
        .update
        .run
  }

  def createUserTable: ZIO[Has[Transactor[Task]], Throwable, Unit] =
    for {
      tnx <- ZIO.service[Transactor[Task]]
      _ <- SQL.createUsersTable.transact(tnx)
    } yield ()

  def dropTable: ZIO[Has[Transactor[Task]], Throwable, Unit] =
    for {
      tnx <- ZIO.service[Transactor[Task]]
      _ <- SQL.dropTable.transact(tnx)
    } yield ()

  import zio.interop.catz.{zioContextShift, zioResourceSyntax}

  def mkTransactor(
                    conf: DatabaseConfig,
                    ec: ExecutionContext,
                    transactEC: ExecutionContext
                  ): Managed[Throwable, HikariTransactor[Task]] =
    HikariTransactor
      .newHikariTransactor[Task](
        driverClassName = conf.driver,
        url = conf.url,
        user = conf.user.getOrElse(""),
        pass = conf.password.getOrElse(""),
        connectEC = ec,
        blocker = Blocker.liftExecutionContext(transactEC)
      )
      .toManagedZIO

  val transactorLive: ZLayer[DBConfig with Blocking, Throwable, Has[Transactor[Task]]] =
    ZLayer.fromManaged(for {
      config     <- URIO.access[DBConfig](_.get).toManaged_
      connectEC  <- ZIO.descriptor.map(_.executor.asEC).toManaged_
      blockingEC <- blocking { ZIO.descriptor.map(_.executor.asEC) }.toManaged_
      transactor <- mkTransactor(config, connectEC, blockingEC)
    } yield transactor)

  val live: ZLayer[Has[Transactor[Task]], Throwable, Has[UserDB]] =
    ZLayer.fromService(
      tnx => new UserDB {
        def create(entity: User): IO[DBError, User] =
          SQL
            .create(entity)
            .transact(tnx)
            .mapError(e => DBError(e.getMessage))

        def get(entityId: Long): IO[DBError, User] =
          SQL
            .get(entityId)
            .transact(tnx)
            .foldM(
              e => IO.fail(DBError(e.getMessage)),
              uo => IO.require(EntityNotFoundError(entityId))(IO.succeed(uo))
            )

        def update(entity: User): IO[DBError, Boolean] =
          SQL
            .update(entity)
            .transact(tnx)
            .bimap(err => DBError(err.getMessage), _ > 0)

        def delete(entityId: Long): IO[DBError, Boolean] =
          SQL
            .delete(entityId)
            .transact(tnx)
            .bimap(err => DBError(err.getMessage), _ > 0)

      }
    )

  type PostgresDBEnvironment = Has[Transactor[Task]] with Has[UserDB]
}
