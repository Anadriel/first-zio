package persistence

import entities.DBError
import zio.IO

object DB {

  trait Service[A] {
    def create(entity: A): IO[DBError, A]

    def get(entityId: Long): IO[DBError, A]

    def update(entity: A): IO[DBError, Boolean]

    def delete(entityId: Long): IO[DBError, Boolean]
  }

}
