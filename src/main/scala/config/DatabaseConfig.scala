package config

final case class DatabaseConfig(
  driver: String,
  url: String,
  user: Option[String],
  password: Option[String],
  maxConnections: Option[Int]
)