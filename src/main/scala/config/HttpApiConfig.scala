package config

final case class HttpApiConfig(
  host: String,
  port: Int
)
