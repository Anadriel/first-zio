package config

import pureconfig.ConfigSource.default.loadOrThrow
import zio.{Has, Task, ZLayer}

case class Config(httpApi: HttpApiConfig, database: DatabaseConfig)

object Config {
  type HttpConfig = Has[HttpApiConfig]
  type DBConfig = Has[DatabaseConfig]

  import pureconfig.generic.auto._

  val live: ZLayer[Any, Throwable, HttpConfig with DBConfig] =
    ZLayer.fromEffectMany(
      Task.effect(loadOrThrow[Config])
        .map(c => Has(c.httpApi) ++ Has(c.database))
    )
}
