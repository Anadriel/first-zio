import config.Config.{DBConfig, HttpConfig}
import config.Config
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import persistence.Postgres.PostgresDBEnvironment
import zio._
import zio.console._
import persistence.{Postgres, UserDB}
import zio.blocking.Blocking
import zio.clock.Clock
import zio.interop.catz._

object MyFirstZioApp extends zio.App {

  type AppEnvironment = HttpConfig with DBConfig with Clock with Console with PostgresDBEnvironment

  type AppTask[A] = RIO[AppEnvironment, A]

  val appEnvironment = Config.live >+> Blocking.live >+> UserDB.postgresLive

  def logic: ZIO[AppEnvironment, Throwable, Unit] =
    for{
      _ <- Postgres.createUserTable
      httpConfig <- ZIO.access[HttpConfig](_.get)
      httpApp = Router[AppTask](
        "/users" -> http.Api().route
      ).orNotFound

      server <- ZIO.runtime[AppEnvironment].flatMap { implicit rts =>
        BlazeServerBuilder[AppTask](scala.concurrent.ExecutionContext.Implicits.global)
          .bindHttp(httpConfig.port, httpConfig.host)
          .withHttpApp(CORS(httpApp))
          .serve
          .compile[AppTask, AppTask, cats.effect.ExitCode]
          .drain
      }
    } yield server

  override def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    logic
      .provideSomeLayer[ZEnv](appEnvironment)
      .tapError(err => putStrLn(s"Execution failed with: $err"))
      .exitCode
  }
}