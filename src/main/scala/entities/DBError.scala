package entities

class DBError(message: String) extends Throwable(message)

object DBError{
  def apply(message: String) = new DBError(message)
}

case class EntityNotFoundError(entityId: Long) extends DBError(s"Entity with id $entityId not found")
