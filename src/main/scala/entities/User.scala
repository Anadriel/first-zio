package entities

case class User(
 id: Option[Long],
 firstName: String,
 lastName: String,
 contacts: String
)
